
function sd {
  $setting_json = '~\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json';
  $obj = get-content $setting_json -raw | ConvertFrom-Json
  $obj.profiles.list[3].startingDirectory=(Get-location).Path
  $obj | convertto-json -depth 100 | Out-file -encoding ascii $setting_json
}

function refresh_user_env {
   $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}
